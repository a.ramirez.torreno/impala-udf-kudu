#ifndef IMPALA_UDF_SAMPLE_UDF_H
#define IMPALA_UDF_SAMPLE_UDF_H

#include <impala_udf/udf.h>

using namespace impala_udf;

BooleanVal ImpalaUserShortNameUDF(FunctionContext* context,const StringVal& idbName,const StringVal& itableName, const StringVal& icolumn);

#endif
