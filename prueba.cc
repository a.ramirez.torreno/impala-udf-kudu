#include <iostream>
#include <string>
#include <algorithm>

void reverseStr(std::string& str)
{
  
  int n = str.length();
  
  for (int i = 0; i < n/2; i++)
  {
   
    std::swap(str[i],str[n - i - 1]);
  }
  
}

int main()
{
  std::string lista = "melon";
  std::string uri ="";
  reverseStr(lista);
  std::cout << lista  << std::endl;
  return 0;
} 
