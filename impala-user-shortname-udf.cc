#include "impala-user-shortname-udf.h"
#include <ctime>
#include <iostream>
#include <sstream>
#include <algorithm>

#include "kudu/client/callbacks.h"
#include "kudu/client/client.h"
#include "kudu/client/row_result.h"
#include "kudu/client/stubs.h"
#include "kudu/client/value.h"
#include "kudu/client/write_op.h"
#include "kudu/common/partial_row.h"
#include "kudu/util/monotime.h"

using kudu::client::KuduClient;
using kudu::client::KuduClientBuilder;
using kudu::client::KuduColumnSchema;
using kudu::client::KuduError;
using kudu::client::KuduInsert;
using kudu::client::KuduPredicate;
using kudu::client::KuduScanBatch;
using kudu::client::KuduScanner;
using kudu::client::KuduSchema;
using kudu::client::KuduSchemaBuilder;
using kudu::client::KuduSession;
using kudu::client::KuduStatusFunctionCallback;
using kudu::client::KuduTable;
using kudu::client::KuduTableAlterer;
using kudu::client::KuduTableCreator;
using kudu::client::KuduValue;
using kudu::client::sp::shared_ptr;
using kudu::KuduPartialRow;
using kudu::MonoDelta;
using kudu::Status;

using std::ostringstream;
using std::string;
using std::vector;

using namespace std;

bool found  = false;


void reverseStr(std::string& str)
{

  int n = str.length();

  for (int i = 0; i < n/2; i++)
  {

    std::swap(str[i],str[n - i - 1]);
  }

}






static Status CreateClient(const vector<string>& master_addrs,
                           std::tr1::shared_ptr<KuduClient>* client) {
  return KuduClientBuilder()
      .master_server_addrs(master_addrs)
      .default_admin_operation_timeout(MonoDelta::FromSeconds(20))
      .Build(client);
}

static Status ScanRows2(const std::tr1::shared_ptr<KuduTable>& table,std::string dbname, std::string tableName,std::string column, std::string user) {
  

  KuduScanner scanner(table.get());

  // To be guaranteed results are returned in primary key order, make the
  // scanner fault-tolerant. This also means the scanner can recover if,
  // for example, the server it is scanning fails in the middle of a scan.
  KUDU_RETURN_NOT_OK(scanner.SetFaultTolerant());
  
  // Add a predicate: WHERE db_name = dbname
  KuduPredicate* p = table->NewComparisonPredicate(
      "db_name", KuduPredicate::EQUAL, KuduValue::CopyString(dbname));
  KUDU_RETURN_NOT_OK(scanner.AddConjunctPredicate(p));

  p = table->NewComparisonPredicate(
      "table_name", KuduPredicate::EQUAL, KuduValue::CopyString(tableName));
  KUDU_RETURN_NOT_OK(scanner.AddConjunctPredicate(p));
 
  p = table->NewComparisonPredicate(
      "column_name", KuduPredicate::EQUAL, KuduValue::CopyString(column));
  KUDU_RETURN_NOT_OK(scanner.AddConjunctPredicate(p));

  p = table->NewComparisonPredicate(
      "uid", KuduPredicate::EQUAL, KuduValue::CopyString(user));
  KUDU_RETURN_NOT_OK(scanner.AddConjunctPredicate(p));


  KUDU_RETURN_NOT_OK(scanner.Open());
  KuduScanBatch batch;

   //int next_row = kLowerBound;
  int nrow = 0;
  while (scanner.HasMoreRows()) {
    KUDU_RETURN_NOT_OK(scanner.NextBatch(&batch));
    for (KuduScanBatch::const_iterator it = batch.begin();
         it != batch.end();
         ++it) {
      KuduScanBatch::RowPtr row(*it);
      nrow++;

    }
  }
  //std:string salida = "";
  //if (nrow == 0) salida = "no encontrado";
  if (nrow > 0) found  = true;


  return Status::OK();
}

void reverseStr(string str, string reversed)
{
  int n = str.length() - 1;
  int cnt = 0;
  for (int i = n; i >= 0; i--)
  {
     reversed[cnt] = str[i];
     cnt++;
  }
}

BooleanVal ImpalaUserShortNameUDF(FunctionContext* context,const StringVal& idbName,const StringVal& itableName, const StringVal& icolumn) {
  vector<string> master_addrs;
  std::string dbName((const char *)idbName.ptr,idbName.len);
  std::string tableName((const char *)itableName.ptr,itableName.len);
  std::string columnName((const char *)icolumn.ptr,icolumn.len);
 // DEVELOPMENT
 // master_addrs.push_back("oclrh70c093.isbcloud.isban.corp");
 //PRE
    master_addrs.push_back("isblcnclomi0001.scisb.isban.corp");
    master_addrs.push_back("isblcnclomi0004.scisb.isban.corp");
    master_addrs.push_back("isblcnclomi0005.scisb.isban.corp");

  const string kTableName = "impala::security.sentry_user_security_column";
 
  std::tr1::shared_ptr<KuduClient> client;
  KUDU_CHECK_OK(CreateClient(master_addrs, &client));
  std::tr1::shared_ptr<KuduTable> table;
  KUDU_CHECK_OK(client->OpenTable(kTableName, &table));
  std::string username(context->effective_user());
  std::string userToSearch = "";
  int pos = username.find("@");
  if (pos == string::npos){
     userToSearch = username;
  } else {
     userToSearch = username.substr(0,pos);
  }
  
  reverseStr(userToSearch);
  KUDU_CHECK_OK(ScanRows2(table,dbName,tableName,columnName,userToSearch));
  
  return found;
  /*
  string username2(context->effective_user());
  string username = salida + username2;
  int pos = username.find("@");
  if (pos == string::npos) {
    return StringVal(username.c_str());
  } else {
    return StringVal(username.substr(0, pos).c_str());
  }
  */
}


